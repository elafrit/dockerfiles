set laststatus=2
set statusline=%{GitBranch()}
set history=1000

set nohlsearch
set nocompatible
set bs=2 "set backspace to be able to delete previous characters"Enable line numbering, taking up 6 spaces
set number

"Turn off word wrapping
set wrap!

"Turn on smart indent
set smartindent
set tabstop=4 "set tab character to 4 characters
set expandtab "turn tabs into whitespace
set shiftwidth=4 "indent width for autoindent
filetype indent on "indent depends on filetype

"Shortcut to auto indent entire file
nmap <F11> 1G=G
imap <F11> <ESC>1G=Ga

nmap <C-Bslash> :bn<CR>

"Map gsi scheme send line
" let @q = ^W^W
map <C-g> @q@o
imap <C-g> <esc>@q@oa
vmap <C-g> @q@m

"Turn on incremental search with ignore case (except explicit caps)
set incsearch
set ignorecase
set smartcase

"Informative status line
"set statusline=%F%m%r%h%w\ [TYPE=%Y\ %{&ff}]\ [%l/%L\ (%p%%)]
set statusline=[%{GitBranch()}]%f%m%r%h%w\ [%Y]\ [%l/%L,%c%V\ (%p%%)]

" Underline the line the cursor is on
" set cursorline

" Make Ctrl-T go to the next vim-tab
nmap <C-T> <ESC>:tabn<CR>

" VIM-Shell
" Ctrl_W e opens up a vimshell in a horizontally split window
" Ctrl_W E opens up a vimshell in a vertically split window
" The shell window will be auto closed after termination
" nmap <C-W>e :new | vimshell bash<CR>
" nmap <C-W>E :vnew | vimshell bash<CR>

"Set color scheme
set t_Co=256
colorscheme desert256
syntax enable

"Enable indent folding
set foldenable
set fdm=marker

"Set space to toggle a fold
nnoremap <space> za

"Hide buffer when not in window (to prevent relogin with FTP edit)
set bufhidden=hide

"Have 3 lines of offset (or buffer) when scrolling
set scrolloff=3

" VIM-Shell
" Ctrl_W e opens up a vimshell in a horizontally split window
" Ctrl_W E opens up a vimshell in a vertically split window
" The shell window will be auto closed after termination
nmap <C-W>e :new \| vimshell bash<CR>
nmap <C-W>E :vnew \| vimshell bash<CR>

" Useful for noweb
au BufRead,BufNewFile *.nw set filetype=noweb

let noweb_backend = 'tex'
let noweb_language = 'python'
let noweb_fold_code = 1

" Useful for asymptote
augroup filetypedetect
    au BufNewFile,BufRead *.asy     setf asy
augroup END

augroup filetypedetect
  au! BufRead,BufNewFile *.spyx,*.pyx set filetype=cython syntax=pyrex
augroup END

" For latex
augroup filetypedetect
    au BufNewFile,BufRead *.nw setlocal spell spelllang=en
augroup END

filetype plugin on

" REQUIRED. This makes vim invoke Latex-Suite when you open a tex file.
filetype plugin on

" IMPORTANT: grep will sometimes skip displaying the file name if you
" search in a singe file. This will confuse Latex-Suite. Set your grep
" program to always generate a file-name.
set grepprg=grep\ -nH\ $*

" OPTIONAL: This enables automatic indentation as you type.
" filetype indent on

" OPTIONAL: Starting with Vim 7, the filetype of empty .tex files defaults to
" 'plaintex' instead of 'tex', which results in vim-latex not being loaded.
" The following changes the default filetype back to 'tex':
let g:tex_flavor='latex'

set foldtext=MyFoldText()
function! MyFoldText()
    let line = getline(v:foldstart)
    let sub = substitute(line, '/\*\|\*/\|{{{.\|%%\d\=', '', 'g')
    return v:folddashes . v:foldlevel . sub
endfunction

command! -nargs=* V !/home/yann/Multiverse/Programming/Scripts/html/myvee.sh <args>

"nds for literate programming
"command!  Not w | execute 'silent !notangle -R%:s/nw/py/ begin.nw %:p end.nw > %:p:s/nw/py/' | redraw!
command!  Not w | execute 'silent !notangle -R%:s/nw/cu/ begin.nw %:p end.nw > %:p:s/nw/cu/' | redraw!
command!  Not2 w | execute 'silent !notangle -R%:s/nw/jl/ begin.nw %:p end.nw > %:p:s/nw/jl/' | redraw!
command!  Now w | execute 'silent !cat begin.nw % end.nw > all% ;noweave -delay -filter disambiguate -index all% > %:s/nw/tex/ ;pdflatex "\scrollmode \input" %:p:s/nw/tex/;while grep -s "here were undefined Sage" %:p:s/nw/log/; do /home/mariem/Sage/Sage-4.8/sage %:p:s/nw/sagetex\.sage/;pdflatex "\scrollmode \input" %:p:s/nw/tex/; done;bibtex %:p:s/nw/tex/;pdflatex %:p:s/nw/tex/;pdflatex %:p:s/nw/tex/'| redraw!
command!  NowSage w | execute 'silent !cat begin.nw % end.nw > all% ;noweave -delay -filter disambiguate -index all% > %:s/nw/tex/ ;pdflatex "\scrollmode \input" %:p:s/nw/tex/;/home/mariem/Sage/Sage-4.8/sage %:p:s/nw/sagetex\.sage/;while grep -s "here were undefined Sage" %:p:s/nw/log/; do /home/mariem/Sage/Sage-4.8/sage %:p:s/nw/sagetex\.sage/;pdflatex "\scrollmode \input" %:p:s/nw/tex/; done;bibtex %:p:s/nw/tex/;pdflatex %:p:s/nw/tex/;pdflatex %:p:s/nw/tex/'| redraw!

""" WORKS BEFORE ADDING SAGE command!  Now w | execute 'silent !cat begin.nw % end.nw > all%;noweave -delay -filter disambiguate -index all% > %:p:s/%/all%/:s/nw/tex/;pdflatex %:p:s/%/all%/:s/nw/tex/;bibtex %:p:s/%/all%/:s/nw/tex/;pdflatex %:p:s/%/all%/:s/nw/tex/;pdflatex %:p:s/%/all%/:s/nw/tex/'| redraw!

map <F2> :Not<CR>
map <F3> :Now<CR>
map <F4> :NowSage<CR>
map <F5> :Not2<CR>
imap <F2> <C-o>:Not<CR>
imap <F3> <C-o>:Now<CR>
imap <F4> <C-o>:NowSage<CR>
imap <F5> <C-o>:Not2<CR>

" Load lwebfold
let b:docFolds = 1
let g:noLwebAutoFolding=1
let g:noLwebManualFolding=1
au BufNewFile,BufReadPre *.nw source ~/.vim/lwebfold.vim

"Pour desactiver les fleches du clavier
"map <right> <esc>
"map <left> <esc>
"map <up> <esc>
"map <down> <esc>
"imap <right> <esc>
"imap <left> <esc>
"imap <up> <esc>
"imap <down> <esc>

let &printexpr="(v:cmdarg=='' ? ".
    \"system('lpr' . (&printdevice == '' ? '' : ' -P' . &printdevice)".
    \". ' ' . v:fname_in) . delete(v:fname_in) + v:shell_error".
    \" : system('mv '.v:fname_in.' '.v:cmdarg) + v:shell_error)"

" Pathogen
execute pathogen#infect()
call pathogen#helptags() " generate helptags for everything in ‘runtimepath’
syntax on
filetype plugin indent on

" Markdown 
let g:markdown_fenced_languages = ['cpp','python','rust']
set exrc
